$(function () {
			    Highcharts.chart('tb-container', {
			        chart: {
			            type: 'column',
			            options3d: {
			                enabled: true,
			                alpha: 10,
			                beta: 25,
			                depth: 70
			            }
			        },
			        title: {
			            text: 'Overall API Analytics'
			        },
			        subtitle: {
			            text: 'Graph showing the popularity of different API endpoints'
			        },
			        plotOptions: {
			            column: {
			                depth: 25
			            }
			        },
			        xAxis: {
			        	title:{

			        		text: 'API Methods'
			        	},
			            categories: [<% for(var i=0; i<count.length; i++ ){ %> ' <%= count[i]["_id"]  %> ',<% } %>]
			        },
			        yAxis: {
			            title: {
			                text: 'View Count'
			            }
			        },
			        series: [{
			            name: 'View Count',
			            data: [<% for(var i=0; i<count.length; i++ ){ %>  <%= count[i]["viewCount"]  %>,<% } %>]
			        }]
			    });
			});

			$(function () {

			    $(document).ready(function () {

			        // Build the chart
			        Highcharts.chart('pie-container', {
			            chart: {
			                plotBackgroundColor: null,
			                plotBorderWidth: null,
			                plotShadow: false,
			                type: 'pie'
			            },
			            title: {
			                text: '% total of View Count for all methods'
			            }, 
			            tooltip: {
			                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
			            },
			            plotOptions: {
			                pie: {
			                    allowPointSelect: true,
			                    cursor: 'pointer',
			                    dataLabels: {
			                        enabled: false
			                    },
			                    showInLegend: true
			                }
			            },
			            series: [{
			                name: 'View Count',
			                colorByPoint: true,
			                data: [

			                <% for(var i=0; i<count.length; i++ ){ %>  { name: '<%= count[i]["_id"] %>' , y:<%= (count[i]["viewCount"]/total * 100).toFixed(2)  %>},<% } %>

			                ]
			            }]
			        });
			    });
			});