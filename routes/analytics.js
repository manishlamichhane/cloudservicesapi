var express = require('express');
var router = express.Router();
var app = require('../app');

/*shows the contents of analytics collection in 3D graph*/
router.get('/',function(req,res){

	var db = req.db;
	var analytics = db.get('analyticCollection');
	analytics.aggregate([{$group : { _id : '$page', viewCount : {$sum : 1} } } , { $sort: { count: -1 } }],function(err,result){

		if(result){

			/*calculating the percentage of view count of each method */

			var total = 0;
			for(var i = 0; i < result.length; i++)
				total += result[i]["viewCount"];

			for(var i=0; i < result.lenght; i++)
				//result[i]['percent'] = (result[i]["viewCount"]/total * 100).toFixed(2);
				result[i]['percent'] = "test";

			console.log(result);
			res.render('analytics/index',{ title : "Analytics", count : result , total : total});	

		}else{
			console.log(result);
			res.send("Error");
		}

	});

});

/*shows the contents of analytics collection in 3D graph*/
router.get('/individual',function(req,res){

	var db = req.db;
	var analytics = db.get('analyticCollection');
	analytics.aggregate([{$group : { _id : '$page', viewCount : {$sum : 1} } } , { $sort: { count: -1 } }],function(err,result){

		if(result){

			res.render('analytics/index',{ title : "Analytics", count : result });	

		}else{
			console.log(result);
			res.send("Error");
		}

	});

});


module.exports = router;