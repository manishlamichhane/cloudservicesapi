var express = require('express');
var router = express.Router();
var app = require('../app');

var moment = require('moment');

/*var mongo = require('mongodb');
var monk = require('monk');
var db = monk('localhost:27017/cloudservies_nosql_db');*/


/* Displays an add form */
router.get('/',function(req,res){

    res.render('cms/add-event',{ title : "Add Event"});

});


/* adds data to database */
router.post('/add',function(req,res){

	var db = req.db;
	var collection = db.get('eventCollection');
	var eventData = {

					'nameOfEvent': req.body.nameOfEvent,
				    'location': req.body.location,
				    'fromDate': req.body.fromDate,
				    'toDate': req.body.toDate,
				    'fromTime': req.body.fromTime,
				    'toTime': req.body.toTime,
				    'ticketPrice': req.body.ticketPrice,
				    'description': req.body.description
				};

	
	collection.insert(eventData,function(err,succ){

		if(err)
			return err;
		else
			res.send("Data inserted successfully!");

	});

    /*res.render('addEvent',{ title : "Add Event"});*/


});



module.exports = router;