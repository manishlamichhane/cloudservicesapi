var express = require('express');
var router = express.Router();
var app = require('../app');

var moment = require('moment');

/*To define database connection in this page*/
/*var mongo = require('mongodb');
var monk = require('monk');
var db = monk('localhost:27017/cloudservies_nosql_db');*/



/*Method to update analytics*/
function updateAnalytics(db,page){

    var analyticCollection = db.get('analyticCollection');
    var analyticData =  {

                            'page' : page,
                            'visited' : true,
                            'timestamp' :  moment().format("YYYY-MM-DD hh:mm A")
                        };

    analyticCollection.insert(analyticData,function(err,succ){
        if(err)
            console.log("Analytics insertion error");
        else
            console.log("Analytics insertion success");

    });

}



router.get('/',function(req,res){

    

    var db = req.db;
    /*Inserting the analytics data to analytics collection*/
    updateAnalytics(db,'/all');
    var collection = db.get('eventCollection');
    collection.find({},{},function(e,docs){
        /*res.send(docs);*/
        if(!Object.keys(docs).length)
            res.send("Sorry! No data found");
        else
            res.render('index',{ title : "Skelleftea Events API", data : JSON.stringify(docs, null, 2)} );
            
    });    
    
    


});

/* Returns all events */
router.get('/all',function(req,res){

	var db = req.db;
    /*Inserting the analytics data to analytics collection*/
    updateAnalytics(db,'/all');
    var collection = db.get('eventCollection');
    collection.find({},{},function(e,docs){
        /*res.send(docs);*/
        if(!Object.keys(docs).length)
        	res.send("Sorry! No data found");
        else
        	res.send(JSON.stringify(docs, null, 2));
    });

});

/* Returns next day event(s) */
router.get('/tomorrow',function(req,res){

	var db = req.db;
    /*Inserting the analytics data to analytics collection*/
    updateAnalytics(db,'/tomorrow');
	var tomorrow = moment().add(1,'days').format("MM/DD/YYYY");
	console.log(tomorrow);
	var collection = db.get('eventCollection');
    collection.find({'fromDate': tomorrow },function(e,docs){
        if(!Object.keys(docs).length)
        	res.send("Sorry! No data found");
        else
        	res.send(JSON.stringify(docs, null, 2));
    });

});

/*Returns next hour event(s) */
router.get('/nexthour',function(req,res){

	var db = req.db;
    /*Inserting the analytics data to analytics collection*/
    updateAnalytics(db,'/nexthour');
	var nexthour = moment().add(1,'hours').format("hh:mm A");
	var collection = db.get('eventCollection');
    collection.find({'fromTime': nexthour},function(e,docs){
        if(!Object.keys(docs).length)
        	res.send("Sorry! No data found");
        else
        	res.send(JSON.stringify(docs, null, 2));
    });		

});

/*Returns the most expensive event in terms of ticket price*/
/*Mongo can't sort numbers stored as strings so this feature will be discussed about later*/
router.get('/priciest',function(req,res){
	var db = req.db;
    /*Inserting the analytics data to analytics collection*/
    updateAnalytics(db,'/priciest');
	var collection = db.get('eventCollection');
    collection.find({},{ "sort": { "ticketPrice": -1 } },function(e,docs){
        if(!Object.keys(docs).length)
        	res.send("Sorry! No data found");
        else
        	res.send(JSON.stringify(docs[0], null, 2));
    });

});

router.get('/location/',function(req,res){
	res.send("Sorry! There is incomplete parameter provided in url. Please add location as well!");
});

/*Returns the event details based on the locations*/
router.get('/location/:name',function(req,res){
	var db = req.db;
    /*Inserting the analytics data to analytics collection*/
    updateAnalytics(db,'/location');
	var locationName = req.params.name;
	var collection = db.get('eventCollection');
    collection.find({"location":locationName},function(e,docs){
        if(!Object.keys(docs).length)
        	res.send("Sorry! No data found");
        else
        	res.send(JSON.stringify(docs, null, ' '));
    });

});

/*Returns the most expensive event in terms of ticket price*/
/*Mongo can't sort numbers stored as strings so this feature will be discussed about later*/
router.get('/cheapest',function(req,res){
    var db = req.db;
    /*Inserting the analytics data to analytics collection*/
    updateAnalytics(db,'/cheapest');
    var collection = db.get('eventCollection');
    collection.find({},{ "sort": { "ticketPrice": 1 } },function(e,docs){
        if(!Object.keys(docs).length)
            res.send("Sorry! No data found");
        else
            res.send(JSON.stringify(docs[0], null, 2));
    });

});


module.exports = router;
