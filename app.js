

var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

/*var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/cloudservies_nosql_db');
var db = mongoose.connection;*/

var mongo = require('mongodb');
var monk = require('monk');
var db = monk('localhost:27017/cloudservies_nosql_db');

/*console.log(db);*/

var routes = require('./routes/index');
var users = require('./routes/user');
var api = require('./routes/api');
var cms = require('./routes/cms');
var analytics = require('./routes/analytics');

var app = express();
/*app.locals.db = db;*/

var env = process.env.NODE_ENV || 'development';
app.locals.ENV = env;
app.locals.ENV_DEVELOPMENT = env == 'development';

// view engine setup

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// app.use(favicon(__dirname + '/public/img/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


// Make our db accessible to our router 
// this part should always be above the route
app.use(function(req,res,next){
    req.db = db;
    next();
});
    

app.use('/', routes);
app.use('/users', users);
app.use('/api', api);
app.use('/cms', cms);
app.use('/analytics', analytics);



/// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});



/// error handlers

// development error handler
// will print stacktrace

if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err,
            title: 'error'
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {},
        title: 'error'
    });
});


module.exports = app;
